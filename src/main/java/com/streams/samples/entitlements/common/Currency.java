package com.streams.samples.entitlements.common;

public enum Currency {
    AUD,
    USD,
    EGP
}
