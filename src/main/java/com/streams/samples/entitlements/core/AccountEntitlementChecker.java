package com.streams.samples.entitlements.core;

import com.streams.samples.entitlements.model.EntitlementCheckCompleted;
import com.streams.samples.entitlements.model.EntitlementCheckResultStatus;
import com.streams.samples.entitlements.model.PaymentReceived;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class AccountEntitlementChecker {

    public EntitlementCheckCompleted checkPayment(PaymentReceived paymentReceived) {
        var checkResultStatus = EntitlementCheckResultStatus.Pass;

        if (isValidAccount(paymentReceived.getFromAccount())) {
            checkResultStatus = EntitlementCheckResultStatus.InvalidCustomerAccount;
        }

        return new EntitlementCheckCompleted(paymentReceived.getPaymentId(), checkResultStatus);
    }

    private boolean isValidAccount(String fromAccount) {
        return fromAccount.toLowerCase().startsWith("blocked_");
    }
}
