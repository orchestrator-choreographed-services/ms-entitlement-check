package com.streams.samples.entitlements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;

@SpringBootApplication
@EnableSchemaRegistryClient
public class EntitlementCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntitlementCheckApplication.class, args);
	}

}
