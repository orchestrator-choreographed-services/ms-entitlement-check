package com.streams.samples.entitlements.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntitlementCommandSignal {
    private String commandPayload;

    public EntitlementCommandSignal() {
    }

    public EntitlementCommandSignal(String commandPayload) {
        this.commandPayload = commandPayload;
    }

    public String getCommandPayload() {
        return commandPayload;
    }

    public void setCommandPayload(String commandPayload) {
        this.commandPayload = commandPayload;
    }
}
