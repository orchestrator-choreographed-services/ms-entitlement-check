package com.streams.samples.entitlements.model;

public enum EntitlementCheckResultStatus {
    Pass,
    InvalidCustomerAccount;
}
