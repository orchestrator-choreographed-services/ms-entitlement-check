package com.streams.samples.entitlements.model;

import lombok.NonNull;
import lombok.Value;

import java.util.Optional;

@Value
public class EntitlementCheckCompleted {
    //This field is added only to support passing constant value to Zeebe. Zeebe connector supports extracting values from
    //fields, it does not support passing constant value
    @NonNull
    private final String paymentId;
    @NonNull
    private EntitlementCheckResultStatus resultStatus;
    //Same as paymentId. This field is added only to support Zeebe connector
    @NonNull
    private final String eventType = "EntitlementCheckCompleted";
}
