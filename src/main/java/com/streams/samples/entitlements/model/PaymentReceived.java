package com.streams.samples.entitlements.model;

import com.streams.samples.entitlements.common.Currency;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;

@Data
@NoArgsConstructor
public class PaymentReceived {
    @NonNull
    private String fromAccount;
    @NonNull
    private String toAccount;
    @NonNull
    private String amount;
    @NonNull
    private Currency currency;
    @NonNull
    private String paymentId;
}
