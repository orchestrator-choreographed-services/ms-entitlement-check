package com.streams.samples.entitlements.processor;

import com.streams.samples.entitlements.core.AccountEntitlementChecker;
import com.streams.samples.entitlements.model.EntitlementCheckCompleted;
import com.streams.samples.entitlements.model.EntitlementCommandSignal;
import com.streams.samples.entitlements.model.PaymentReceived;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Slf4j
@EnableBinding(PaymentReceivedProcessor.StreamProcessor.class)
public class PaymentReceivedProcessor {
    @Autowired
    private AccountEntitlementChecker entitlementChecker;

    @StreamListener
    @SendTo("entitlementCheckCompleted")
    public KStream<String, EntitlementCheckCompleted> process(@Input("paymentReceived") KStream<String, PaymentReceived> paymentReceived,
                                                              @Input("entitlementCommandSignal") KStream<String, EntitlementCommandSignal> commandSignal) {
        paymentReceived.peek((k, v) -> {
            log.info("Received a payment created event with payment ID {}", k);
        });

        commandSignal.peek((k, v) -> {
            log.info("Received a command signal for payment with ID {}", k);
        });

        JoinWindows joinWindow = JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());
        return paymentReceived.join(commandSignal, (v1, v2) -> v1, joinWindow, joined())
                .mapValues((k, v) -> entitlementChecker.checkPayment(v));
    }

    private Joined<String, PaymentReceived, EntitlementCommandSignal> joined() {
        return Joined.with(new Serdes.StringSerde(), new JsonSerde<>(PaymentReceived.class), new JsonSerde<>(EntitlementCommandSignal.class));
    }

    public interface StreamProcessor {
        @Input("paymentReceived")
        KStream<?, ?> paymentReceived();

        @Input("entitlementCommandSignal")
        KStream<?, ?> entitlementCommandSignal();

        @Output("entitlementCheckCompleted")
        KStream<?, ?> entitlementCheckCompleted();
    }
}
