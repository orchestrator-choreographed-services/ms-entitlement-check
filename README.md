# Entitlement check

This service checks the creditor account(from account). 
Any account starting with `blocked_` will be considered invalid and the service will return `InvalidCustomerAccount`.

## Prerequisite

Before running this service please make sure you have the following

* Start all required services by following the steps in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure)
* Make sure the required topics are pre-created in the Kafka cluster you are using

## Start the service

To start the service, you could use `gradle bootrun`.

## Publish payments

To publish PaymentCreated event, please use the scripts in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure/blob/master/publish_payments.sh)

